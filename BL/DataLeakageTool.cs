﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using SharedClasses;

namespace BL
{
    public class DataLeakageTool
    {
            public OrderFile[] scanFiles(String address)
            {
                string[] filePaths = Directory.GetFiles(address);//Gets the path to the files in the adress the account chose.
                DirectoryInfo direct = new DirectoryInfo(address);
                FileInfo[] Infos = direct.GetFiles("*.txt");//Gets all the information of txt files.
                double score = 0;
                int numOfWords = 0;
                double result;
                OrderFile[] order = new OrderFile[Infos.Length];//Array in the length of the number of txt files in the folder.
                char[] delimeters = { ' ', ',', '.', '/', ':', '@', '?', '!', '(', ')', '~', '%', '#', '$', '<', '>', '[', ']' };//Split with those delimeters.
                int textIndex = 0;//Index in the texts Array -order.
                for (int h = 0; h < filePaths.Length; h++)//Move on all the files.
                {
                    if (filePaths[h].EndsWith(".txt"))//Do procces just on the txt files and ignore others.
                    {
                        string[] lines = File.ReadAllLines(filePaths[h]);//Convert the file into strings array of his lines.
                        for (int i = 0; i < lines.Length; i++)//Move each on line.
                        {
                            string[] words = lines[i].Split(delimeters, StringSplitOptions.RemoveEmptyEntries);//Split line to words with the delimeters.
                            for (int j = 0; j < words.Length; j++)
                            {
                                score = score + scoring(words[j]);//Compute score for every word.
                                numOfWords++;//Increment the number of words.
                            }
                        }
                        if (lines.Length != 0)//If the file is not empty.
                        {
                            result = score / numOfWords;//The result is the average between score and number of words.
                            order[textIndex] = new OrderFile(Infos[textIndex].Name, result, filePaths[h]);//New orderfile with name,result and path.
                            textIndex++;
                            score = 0;
                            numOfWords = 0;
                        }
                        else//If the file is empth.
                        {
                            result = 0;//Empty file has result of zero.
                            order[textIndex] = new OrderFile(Infos[textIndex].Name, result, filePaths[h]);
                            textIndex++;
                            score = 0;
                            numOfWords = 0;
                        }
                    }
                }
                return order;//Return the array of orders files - by their order in the folder.
            } 
                   
            private double scoring(String word)//Function for score special words with sensitivity.
            {
                string lowerWord = word.ToLower();//Makes the word with small letters only.
                double score = 0;
                switch (lowerWord)
                {
                     case "classified":
                     score = 1;
                     break;

                     case "secret":
                     score = 1;
                     break;

                     case "password":
                     score = 1;
                     break;

                     case "hash":
                     score = 0.75;
                     break;

                     case "break-in":
                     score = 0.6;
                     break;

                     case "sensitive":
                     score = 0.8;
                     break;

                     case "restricted":
                     score = 1;
                     break;

                     case "encrypt":
                     score = 0.8;
                     break;

                     case "private":
                     score = 0.9;
                     break;

                     case "credential":
                     score = 0.6;
                     break;

                     case "pin":
                     score = 0.8;
                     break;

                     case "key":
                     score = 0.8;
                     break;

                     case "admin":
                     score = 0.6;
                     break;

                     case "virus":
                     score = 0.6;
                     break;

                     case "worm":
                     score = 0.4;
                     break;
             }        
            return score;
        }        
    }
}

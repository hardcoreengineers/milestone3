﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace BL
{
    class EncryptDecrypt
    {
        public EncryptDecrypt(){
            }
        public string EncryptFile(string inputFile, string password)
        {
            try
            {
                string outputFile = inputFile + "temp";
                UnicodeEncoding UE = new UnicodeEncoding();
                byte[] key = UE.GetBytes(password);

                string cryptFile = outputFile;
                FileStream fsCrypt = new FileStream(cryptFile, FileMode.Create);

                RijndaelManaged RMCrypto = new RijndaelManaged();

                CryptoStream cs = new CryptoStream(fsCrypt,
                    RMCrypto.CreateEncryptor(key, key),
                    CryptoStreamMode.Write);

                FileStream fsIn = new FileStream(inputFile, FileMode.Open);

                int data;
                while ((data = fsIn.ReadByte()) != -1)
                    cs.WriteByte((byte)data);


                fsIn.Close();
                cs.Close();
                fsCrypt.Close();
                File.Delete(inputFile);
                System.IO.File.Move(outputFile, inputFile);
                return "File has been encrypted successfully!";
            }
            catch
            {
                return "File encryption has failed!";
            }
        }

        public string DecryptFile(string inputFile, string password)
        {
            try
            {
                string outputFile = inputFile + "temp";

                UnicodeEncoding UE = new UnicodeEncoding();
                byte[] key = UE.GetBytes(password);

                FileStream fsCrypt = new FileStream(inputFile, FileMode.Open);

                RijndaelManaged RMCrypto = new RijndaelManaged();

                CryptoStream cs = new CryptoStream(fsCrypt,
                    RMCrypto.CreateDecryptor(key, key),
                    CryptoStreamMode.Read);

                FileStream fsOut = new FileStream(outputFile, FileMode.Create);

                int data;
                while ((data = cs.ReadByte()) != -1)
                    fsOut.WriteByte((byte)data);

                fsOut.Close();
                cs.Close();
                fsCrypt.Close();
                File.Delete(inputFile);
                System.IO.File.Move(outputFile, inputFile);
                return "File has been decrypted successfully!";
            }
            catch 
            {

                return "File decryption has failed";
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedClasses;

namespace BL
{
    /*
     Interface of Business Layer
      */
    public interface IBL
    {
        bool verify(string username, string password);
        bool changePassword(User currUser, User user,string pass);
        void deleteUser(User currUser, User user);
        void addUser(User currUser, User user);
        void changeRole(User currUser, User user, string newRole);
        bool isExist(string user);
        bool checkPass(string password);
        string getRandomPassword();
        void addLog(Log log);
        List<Log> getLog();
        List<User> getUsersOfRole(string role);
        string getRole(string user);
        OrderFile[] scanFiles(string adress);
        OrderFile[] sortedFiles(OrderFile [] arr);
        string Encryption(string path, string password);
        string Decryption(string path, string password);
    }
}

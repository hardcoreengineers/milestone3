﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedClasses;
using DAL;

namespace BL
{
    class LogManagment
    {
        private IDAL itsadal;
         public LogManagment(IDAL itsadal)
        {
            this.itsadal = itsadal;
        }
        public List<Log> getLogs()
        {
            return itsadal.getLogs();
        }
        public void addLog(Log log)//Adds a new log to the database
        {
            this.itsadal.addLog(log);
        }
    }
}

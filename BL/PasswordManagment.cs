﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL
{
    public static class PasswordManagment
    {
        public static string getRandomPassword()
        {
            int randomNumber = 0;
            Random rnd = new Random(); // generate random varible.
            string randomPass = ""; // creates a password string.
            int mustI = rnd.Next(8); // determines a random places for a must number in the password.
            for (int i = 0; i < 8; i++)
            {
                if (i == mustI)
                {
                    randomNumber = rnd.Next(10); // if the i is the random-must-place for an int than randomize a number.
                    randomPass = randomPass + randomNumber.ToString();// adds the number to password string.
                }
                else
                {
                    randomNumber = rnd.Next(62);// 26 small letters 26 captial letters and 10 numbers.
                    if (randomNumber >= 52 && randomNumber <= 61)// 0~25 are small letters 26~5` captial letters and 52~61 are numbers.
                    {
                        randomNumber = randomNumber - 52;    // taking 52 out of the random if its a number so it will be betweeb 0 and 9.
                        randomPass = randomPass + randomNumber.ToString();
                    }
                    else
                    {
                        if (randomNumber >= 0 && randomNumber <= 25) // if small letter.
                        {
                            randomNumber = randomNumber + (int)'a'; //adding the 'a' unicode number so the rndnum will represent unicode of a small letter.
                            randomPass = randomPass + (char)randomNumber;
                        }
                        else
                        {
                            randomNumber = randomNumber - 26 + (int)'A';// subtracting 26 so the num will be between 0~26 than adding 'A' unicode.
                            randomPass = randomPass + (char)randomNumber;
                        }
                    }
                }
            }
            return randomPass;
        }
        public static bool checkPass(string s)
        {
            if (s.Length == 8)//Checks if the password has 8 characters
            {
                string numbers = "0123456789";
                int counter = 0;
                for (int i = 0; i < s.Length; i++)//Checks if the password has at least one number
                {
                    if (numbers.IndexOf(s[i]) != -1)
                    {
                        counter++;
                    }
                }
                if (counter != 0)
                {
                    return true;
                }
            }
            return false;
        }
    }
}

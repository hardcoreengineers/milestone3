﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using SharedClasses;
using System.IO;

namespace BL
{
    public class UserManagment
    {
        private IDAL itsadal;
        private LogManagment logMan;
        public UserManagment(IDAL dl)
        {
            itsadal = dl;
            logMan = new LogManagment(itsadal);
        }
        public bool verify(string username, string password)//Verifies the username and password
        {
            return this.itsadal.verify(username, password);
        }
        public bool changePassword(User currUser, User user, string newPassword)//currUser changes the password of user to newPassword
        {
            if (PasswordManagment.checkPass(newPassword))//Checks if the password is in the right format.
            {
                itsadal.changePassword(user,newPassword);
                itsadal.addLog(new Log("changed_password", DateTime.Now.ToString("dd/MM/yyyy HH:mm"), currUser.getUsername(), user.getUsername()));
                return true;
            }
            else
            {
                return false;
            }
        }
        public List<User> getUsersOfRole(string role)//Returns all the user of the received role
        {
            return this.itsadal.getUsersOfRole(role);
        }
        public void deleteUser(User currUser, User user)//Deletes a user from the database file.
        {
            itsadal.deleteUser(user);
            itsadal.addLog(new Log("removed_user", DateTime.Now.ToString("dd/MM/yyyy HH:mm"), currUser.getUsername(), user.getUsername()));
        }
        public void addUser(User currUser, User user)//Adds a new user to the database by the currUser.
        {
            this.itsadal.addUser(user);
            itsadal.addLog(new Log("added_user", DateTime.Now.ToString("dd/MM/yyyy HH:mm"), currUser.getUsername(), user.getUsername()));
        }
        public void changeRole(User currUser, User user, string newRole)//Changes the role of the user by currUser.
        {
            this.itsadal.changeRole(user, newRole);
            itsadal.addLog(new Log("changed_role", DateTime.Now.ToString("dd/MM/yyyy HH:mm"), currUser.getUsername(), user.getUsername()));
        }
        public bool isExist(string username)//Checks if the user with the received username exists 
        {
            return itsadal.isExist(username);
        }
        public string getRole(string username)//Return the role of the user with the received username.
        {
            return itsadal.getRole(username);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedClasses;
using DAL;

namespace BL
{
    public class Management : IBL
    {
        private IDAL dataAccess;
        private UserManagment userMan;
        private LogManagment logMan;
        private DataLeakageTool DLT;
        private EncryptDecrypt ED;
        public Management(IDAL dataAccess)
        {
            this.dataAccess = dataAccess;
            userMan = new UserManagment(dataAccess);
            logMan = new LogManagment(dataAccess);
            DLT = new DataLeakageTool();
            ED = new EncryptDecrypt();
        }
        public void addLog(Log log)
        {
            logMan.addLog(log);
        }

        public void addUser(User currUser,User user)
        {
            userMan.addUser(currUser,user);
        }

        public bool changePassword(User currUser,User user, string pass)
        {
           return userMan.changePassword(currUser,user, pass);
        }

        public void changeRole(User currUser,User user, string newRole)
        {
            userMan.changeRole(currUser,user, newRole);
        }

        public bool checkPass(string password)
        {
            return PasswordManagment.checkPass(password);
        }

        public void deleteUser(User currUser,User user)
        {
            userMan.deleteUser(currUser,user);
        }

        public string getRandomPassword()
        {
            return PasswordManagment.getRandomPassword();
        }
        public List<User> getUsersOfRole(string role)
        {
            return userMan.getUsersOfRole(role);
        }
        public bool isExist(string user)
        {
            return userMan.isExist(user);
        }

        public bool verify(string username, string password)
        {
            return userMan.verify(username, password);
        }

        public string getRole(string username)
        {
            return userMan.getRole(username);
        }
        public OrderFile[] scanFiles(string adress)
        {
            return DLT.scanFiles(adress);
        }
        public OrderFile[] sortedFiles(OrderFile[] arr)
        {
            return OrderFile.sortedFiles(arr);
        }
        public string Encryption(string path, string key)
        {
            return ED.EncryptFile(path, key);
        }

        public string Decryption(string path, string key)
        {
            return ED.DecryptFile(path, key);
        }
        public  List<Log> getLog()
        {
            return logMan.getLogs();
        }
    }
}

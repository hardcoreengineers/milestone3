﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using SharedClasses;

namespace DAL
{
    public class DataAccess : IDAL
    {
        private UserAccess userAccess;
        private LogAccess logAccess;
        public DataAccess()
        {
            userAccess = new UserAccess();
            logAccess = new LogAccess();
        }
        public void addLog(Log log)//Changes the logs.
        {
            this.logAccess.insertLog(log);
        }

        public void changeUsers(string[] users)//Changes the users.
        {
            userAccess.changeUsers(users);
        }

        public List<string> getLogs()//Return the logs from the database.
        {
            return logAccess.getLogs();
        }

        public bool verify(string username, string password)
        {
            return userAccess.verify(username, password);
        }

        public List<User> getUsersOfRole(string role)
        {
            return userAccess.getUsersOfRole(role);
        }

        public void changePassword(User user, string newPassword)
        {
            userAccess.changePassword(user, newPassword);
        }

        public void deleteUser(User user)
        {
            userAccess.deleteUser(user);
        }

        public void addUser(User user)
        {
            userAccess.addUser(user);
        }

        public void changeRole(User user, string newRole)
        {
            userAccess.changeRole(user, newRole);
        }

        public bool isExist(string username)
        {
            return userAccess.isExist(username);
        }

        public string getRole(string username)
        {
            return userAccess.getRole(username);
        }
    }
}

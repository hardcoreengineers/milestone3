﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Data.SqlClient;
using SharedClasses;

namespace DAL
{
    class UserAccess
    {
        public UserAccess()
        {

        }
        readonly SqlConnection myConnection = new SqlConnection("user id=DESKTOP-TA7DGMV\\kutig;" + "password=;server=DESKTOP-TA7DGMV\\DATABASE_MIL_3;" + "Trusted_Connection=yes;" + "database=Database_Mile3;" + "connection timeout=30");
        readonly static string address = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "/UserDatabase.txt";
        public bool verify(string username,string password)//Return the user.
        {
            try
            {
                myConnection.Open();
                SqlCommand verifyCommand = new SqlCommand("SELECT * FROM Users WHERE username='"+username+"' AND password='"+password+"'", myConnection);
                SqlDataReader usersReader = verifyCommand.ExecuteReader(); //exceute query and store response in a serilalized
                if(usersReader.HasRows)
                {
                    myConnection.Close();
                    return true;
                }
                else
                {
                    myConnection.Close();
                    return false;
                }
            }
            catch (Exception e)
            {
                myConnection.Close();
                return false;
            }
        }
        public List<User> getUsersOfRole(string role)
        {
            myConnection.Open();
            SqlCommand getCommand = new SqlCommand("SELECT * FROM Users WHERE role='" + role + "'", myConnection);
            SqlDataReader usersReader = getCommand.ExecuteReader(); //exceute query and store response in a serilalized
            List<User> lst = new List<User>();
            while (usersReader.Read()) //while there are more rows to read... move pointer to the next row
            {
                lst.Add(new User(usersReader["username"].ToString(), usersReader["password"].ToString(), usersReader["role"].ToString()));
            }
            myConnection.Close();
            return lst;
        }
        public void changePassword(User user,string newPassword)
        {
            myConnection.Open();
            SqlCommand updateCommand = new SqlCommand("UPDATE Users SET password = '"+newPassword+"' WHERE username = '"+user.getUsername()+"'", myConnection);
            updateCommand.ExecuteNonQuery();
            myConnection.Close();
        }
        public void deleteUser(User user)
        {
            myConnection.Open();
            SqlCommand deleteCommand = new SqlCommand("DELETE FROM Users where username='"+user.getUsername()+"'", myConnection);
            deleteCommand.ExecuteNonQuery();
            myConnection.Close();
        }
        public void addUser(User user)
        {
            myConnection.Open();
            SqlCommand insertCommand = new SqlCommand("INSERT INTO Users (username,password,role)" + "VALUES ('" + user.getUsername() + "','" + user.getPassword() + "', '" + user.getRole() +"')", myConnection);
            insertCommand.ExecuteNonQuery();
            myConnection.Close();
        }
        public void changeRole(User user,string newRole)
        {
            myConnection.Open();
            SqlCommand updateCommand = new SqlCommand("UPDATE Users SET role = '" + newRole + "' WHERE username = '" + user.getUsername() + "'", myConnection);
            updateCommand.ExecuteNonQuery();
            myConnection.Close();
        }
        public bool isExist(string username)
        {
            myConnection.Open();
            SqlCommand verifyCommand = new SqlCommand("SELECT * FROM Users WHERE username='" + username +"'", myConnection);
            SqlDataReader usersReader = verifyCommand.ExecuteReader(); //exceute query and store response in a serilalized
            if (usersReader.HasRows)
            {
                myConnection.Close();
                return true;
            }
            else
            {
                myConnection.Close();
                return false;
            }
        }
        public string getRole(string username)
        {
            myConnection.Open();
            SqlCommand verifyCommand = new SqlCommand("SELECT * FROM Users WHERE username='" + username + "'", myConnection);
            SqlDataReader usersReader = verifyCommand.ExecuteReader(); //exceute query and store response in a serilalized
            usersReader.Read();
            String role= usersReader["role"].ToString();
            myConnection.Close();
            return role;
        }
        public void changeUsers(string[] users)//Changes the users.
        {
            File.WriteAllLines(address, users);
        }
    }
}

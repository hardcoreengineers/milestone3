﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedClasses
{
    public class OrderFile
    {
        public string name { get; set; }
        public string path { get; set; }
        public double score { get; set; }
        public OrderFile(string name, double score, string path)
        {
            this.name = name;
            this.score = score;
            this.path = path;
        }
        public string getName()
        {
            return name;
        }
        public string getPath()
        {
            return path;
        }
        public double getScore()
        {
            return score;
        }
        public override string ToString()
        {
            return "The Score of the File: " + name + " " + "is :" + score;
        }
        public static OrderFile[] sortedFiles(OrderFile[] arr)//Return ordered copy of the orderedfile array.
        {
            OrderFile[] ordered = new OrderFile[arr.Length];
            for (int i = 0; i < arr.Length; i++)
            {
                ordered[i] = arr[i];
            }
            insertionSort(ordered);
            return ordered;
        }
        public static void insertionSort(OrderFile[] arr)//Sort by insertionsort.
        {
            int l = arr.Length;
            int i = 1;
            while (i < l)
            {
                insert(arr, i);
                i = i + 1;
            }
            for (int j = 0; j < l / 2; j++)
            {
                OrderFile temp = arr[j];
                arr[j] = arr[l - 1 - j];
                arr[l - 1 - j] = temp;
            }
        }
        public static void insert(OrderFile[] arr, int i)//part of the insertion sort.
        {
            OrderFile a1 = arr[i];
            double value = arr[i].getScore();
            while (i > 0 && arr[i - 1].getScore() > value)
            {
                arr[i] = arr[i - 1];
                i = i - 1;
            }
            arr[i] = a1;
        }
    }
}

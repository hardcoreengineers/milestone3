﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BL;
using SharedClasses;
namespace PL_GUI
{
    /// <summary>
    /// This page is being called whenever an Admin/manager wants to modify a specific user info
    /// There are 3 options: Remove user,change his password and change his role
    /// </summary>
    public partial class EditUser : UserControl
    {
        IBL myBL;
        User changerUser;
        User changedUser;
        List<string> comboBoxOptions;
        public EditUser(IBL theBL,User changer,User changed)
        {
            myBL = theBL;
            changerUser = changer;
            changedUser = changed;
            comboBoxOptions = new List<string>();
            comboBoxOptions.Add("employee");
            if(changer.getRole().Equals("admin"))
            {
                comboBoxOptions.Add("manager");
                comboBoxOptions.Add("admin");
            }
            
            InitializeComponent();
            RolesComboBox.ItemsSource = comboBoxOptions;
        }

        private void ChangePassword_Button_Click(object sender, RoutedEventArgs e)
        {
            NewPassword NP = new NewPassword(myBL,changerUser,changedUser, "EditUser");
            this.Content = NP;
        }

        private void RemoveUser_Button_Click(object sender, RoutedEventArgs e)
        {
            if(changedUser.getUsername().Equals(changerUser.getUsername()))
            {
                MessageBox.Show("You can't remove yourself!");
            }
            else
            {
                myBL.deleteUser(changerUser, changedUser);
                UserControl uc = new UsersControl(myBL, changerUser);
                this.Content = uc;
            }
        }

        private void RoleUpdate_Button_Click(object sender, RoutedEventArgs e)
        {
            if(RolesComboBox.Text!="")
            {
                myBL.changeRole(changerUser, changedUser, RolesComboBox.Text);
                MessageBox.Show("User role has been changed to " + RolesComboBox.Text);
            }
            else
            {
                MessageBox.Show("Please choose a role");
            } 
        }

        private void GoBack_Click(object sender, RoutedEventArgs e)
        {
            UserControl uc = new UsersControl(myBL, changerUser);
            this.Content = uc;
        }
    }
}

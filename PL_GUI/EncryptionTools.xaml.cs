﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BL;
using SharedClasses;
namespace PL_GUI
{
    /// <summary>
    /// The encryprion frame is where users can choose a file, a key and encrypt/decrypt the file
    /// the key has to be 8 letters long
    /// all files can be choosen
    /// </summary>
    public partial class EncryptionTools : UserControl
    {
        IBL myBL;
        User me;
        public EncryptionTools(IBL myBL,User user)
        {
            this.myBL = myBL;
            me = user;
            InitializeComponent();
        }

        private void FileOpener_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.DefaultExt = ".*";
            dlg.Filter = "All |*.*";
            Nullable<bool> result = dlg.ShowDialog();

            if(result ==true)
            {
                string filename = dlg.FileName;
                FilePath.Text = filename;

                FilePath.Visibility = Visibility.Visible;
                selectOptionLabel.Visibility = Visibility.Visible;
                EncryptButton.Visibility = Visibility.Visible;
                DecryptButton.Visibility = Visibility.Visible;
                EnterKeyLabel.Visibility = Visibility.Visible;
                Key.Visibility = Visibility.Visible;
            }
        }
        private void GoBack_Click(object sender, RoutedEventArgs e)
        {
            MainMenu mm = new MainMenu(myBL, me);
            this.Content = mm;
        }

        private void EncryptButton_Click(object sender, RoutedEventArgs e)
        {
            FileStatus.Content = myBL.Encryption(FilePath.Text, Key.Text);
            FileStatus.Visibility = Visibility.Visible;
        }

        private void DecryptButton_Click(object sender, RoutedEventArgs e)
        {
            FileStatus.Content = myBL.Decryption(FilePath.Text, Key.Text);
            FileStatus.Visibility = Visibility.Visible;
        }

        private void Key_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (Key.Text.Length == 8)
            {
                EncryptButton.IsEnabled = true;
                DecryptButton.IsEnabled = true;
            }
            else
            {
                EncryptButton.IsEnabled = false;
                DecryptButton.IsEnabled = false;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BL;
using SharedClasses;

namespace PL_GUI
{
    /// <summary>
    /// This page allows the user/creator to change the password or generate a random one .
    /// Password must contain 8 characters and at least 1 digit
    /// </summary>
    public partial class NewPassword : UserControl
    {
        private User me;
        private IBL myBL;
        private User originalUser;
        private string backPage;
      /*  public NewPassword(IBL myBL, User me,string backPage)
        {
            this.myBL = myBL;
            this.me = me;
            this.backPage = backPage;
            InitializeComponent();
        }
        */
        public NewPassword(IBL myBL,User originalUser, User changeUser, string backPage)
        {
            this.myBL = myBL;
            this.me = changeUser;
            this.originalUser = originalUser;
            this.backPage = backPage;
            InitializeComponent();
        }
        private void ChangePassword_Click(object sender, RoutedEventArgs e)
        {
            string password = PasswordTextBox.Password;
            if (myBL.changePassword(this.originalUser, me, password))
            {
                MessageBox.Show("Password has changed successfully! ");
            }
            else
            {
                MessageBox.Show("Password must contain 8 characters and and at least one digit.");
            }
        }

        private void RandomPasswordButton_Click(object sender, RoutedEventArgs e)
        {
            String password = myBL.getRandomPassword();
            bool isGood=myBL.changePassword(me,me,password);
            NewPasswordText.Text = password;
            this.RandomPasswordStatus.Visibility = Visibility.Visible;
            this.NewPasswordText.Visibility = Visibility.Visible;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if(backPage.Equals("Account"))
            {
                Account ac = new Account(myBL, me);
                this.Content = ac;
            }
            else if(backPage.Equals("EditUser"))
            {
                UsersControl uc = new UsersControl(myBL, originalUser);
                this.Content = uc;
            }

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using SharedClasses;
using BL;

namespace PL_GUI
{
    /// <summary>
    /// Interaction logic for CreateUser.xaml
    /// </summary>
    public partial class CreateUser : UserControl
    {
        IBL myBL;
        User theCreatorUser;
        List<string> comboBoxOptions;
        public CreateUser(IBL theBL,User creator)
        {
            myBL = theBL;
            theCreatorUser = creator;
            InitializeComponent();
            comboBoxOptions = new List<string>();
            comboBoxOptions.Add("employee");
            comboBoxOptions.Add("manager");
            if(theCreatorUser.role.Equals("admin"))
            {
                comboBoxOptions.Add("admin");
            }
            Role.ItemsSource = comboBoxOptions;
        }

        private void GoBack_Click(object sender, RoutedEventArgs e)
        {
            UsersControl uc = new UsersControl(myBL, theCreatorUser);
            this.Content = uc;
        }

        private void CreateButton_Click(object sender, RoutedEventArgs e)
        {
            if (myBL.checkPass(Password.Password))
            {
                if(Username.Text != "" &&Username.Text!=null && !myBL.isExist(Username.Text))
                {

                        myBL.addUser(theCreatorUser,new User(Username.Text, Password.Password, Role.Text));
                        MessageBox.Show("User has been created successfully!");
                }
                else
                {
                    MessageBox.Show("Invalid Username");
                }
            }
            else
            {
                MessageBox.Show("Password must contain 8 characters and and at least one digit.");
            }

        }
    }
}

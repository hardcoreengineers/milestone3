﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using SharedClasses;
using BL;
using System.Diagnostics;

namespace PL_GUI
{
    /// <summary>
    /// The Data-Leakage tool page lets you choose a Folder.
    /// The user then sees all files and their sensitiviy.
    /// He can also order/disorder them and open them by pressing "Open File" button
    /// </summary>
    public partial class DataLeakageTool : UserControl
    {
        IBL myBL;
        User me;
        OrderFile[] allFiles;
        OrderFile[] orderedAllFiles;
        public DataLeakageTool(IBL theBL,User theUser)
        {
            me = theUser;
            myBL = theBL;
            InitializeComponent();
        }
        public void GoBack_Click(object sender, RoutedEventArgs e)
        {
            MainMenu mm = new MainMenu(myBL, me);
            this.Content = mm;
        }

        private void ChooseFolder_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new System.Windows.Forms.FolderBrowserDialog();
            System.Windows.Forms.DialogResult result = dialog.ShowDialog();

            if (result.ToString().Equals("OK"))
            {
                FolderPath.Text = dialog.SelectedPath.ToString();
                allFiles = myBL.scanFiles(FolderPath.Text);
                orderedAllFiles = myBL.sortedFiles(allFiles);
                OrderButton.Visibility = Visibility.Visible;
                SenstivityGrid.ItemsSource = allFiles;
                SenstivityGrid.Visibility = Visibility.Visible;
                OrderButton.Content = "Order";
            }
        }

        private void OrderButton_Click(object sender, RoutedEventArgs e)
        {
            if (OrderButton.Content.Equals("Order"))
            {
                SenstivityGrid.ItemsSource = orderedAllFiles;
                OrderButton.Content = "Unorder";
                
            }
            else
            {
                SenstivityGrid.ItemsSource = allFiles;
                OrderButton.Content = "Order";
            }
           
        }

        private void OpenFile_Click(object sender, RoutedEventArgs e)
        {
            Process.Start(((OrderFile)SenstivityGrid.SelectedItem).path);
        }
    }
}

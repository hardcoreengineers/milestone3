﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BL;
using SharedClasses;
using System.Threading;

namespace PL_GUI
{
    /// <summary>
    /// The login menu .
    /// Users who failed 5 times to login will get DC
    /// </summary>
    public partial class LoginMenu : UserControl
    {
        IBL myBL;
        User user;
        int LoginTries;
        public LoginMenu(IBL theBL)
        {
            LoginTries = 5;
            myBL = theBL;
            InitializeComponent();

        }

        private void LoginButton_Click(object sender, RoutedEventArgs e) //login verifier
        {
            String username = this.Username.Text;
            String password = this.Password.Password;
            if (myBL.verify(username, password))
            {
                user = new User(username, password,myBL.getRole(username));
                MessageBox.Show("Welcome " + user.getUsername());
                MainMenu mainmenu = new MainMenu(myBL, user);
                this.Content = mainmenu;
            }
            else
            {
                LoginTries--;
                if (LoginTries == 0)
                {
                    
                    MessageBox.Show("No more tries. Program has shutdown");
                    Environment.Exit(0);
                }
                else
                {
                    MessageBox.Show("Wrong Username/Password, " + LoginTries + " tries left. Please try again");
                }
            }
        }
        private void Password_PreviewKeyDown(object sender, KeyEventArgs e)  //enter = login
        {
            if(e.Key == Key.Enter)
            {
                LoginButton_Click(sender, e);
            }
        }
    }
}
